<%@page import="com.sliit.ssd.assignment.GoogleAuthenticator"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"
    import="org.json.JSONObject,org.json.JSONException,java.util.*"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>Google OAuth 2.0 Authentication</title>
<style>
body {
	font-family: Sans-Serif;
}

.center {
				margin: auto;
				width: 60%;
				padding: 10px;
				text-align: center;
}
.headingCenter {
				margin: auto;
				width: 60%;
				padding: 10px;
				text-align: center;
}

</style>
</head>
<body>
	<div style="float:center" class="center">
	    <div class="headingCenter">
        	<h2 style="float:center"> SSD OAuth 2.0</h2>
        </div>
        <div class="headingCenter">
		<%
			final GoogleAuthenticator GA = new GoogleAuthenticator();
			if (request.getParameter("code") == null
					|| request.getParameter("state") == null) {

				out.println("<a href='" + GA.buildLoginUrl()+ "'>Sign in with <i class='fa fa-google' style='font-size:36px;color:red'></i></a>");
				session.setAttribute("state", GA.getStateToken());

			} else if (request.getParameter("code") != null && request.getParameter("state") != null
					&& request.getParameter("state").equals(session.getAttribute("state"))) {

				session.removeAttribute("state");

				out.println("<pre>");

                String value = GA.getGoogleProfile(request.getParameter("code"));
                JSONObject jsonObj = new JSONObject(value);

                String email = (String) jsonObj.get("email");
                String name = (String) jsonObj.get("name");
                String given_name = (String) jsonObj.get("given_name");
                String pictureURL = (String) jsonObj.get("picture");

                out.println("Hello " + given_name);
                out.println("<img src='"+pictureURL+"'/>");
                out.println("Logged in as : " + name);
                out.println("Email: " + email);
				out.println("</pre>");
			}
		%>
                <h2 style="float:center"> SSD OAuth 2.0 | T M S M Tennakoon | IT13099114</h2>
		</div>
	</div>
	<br />
</body>
</html>
